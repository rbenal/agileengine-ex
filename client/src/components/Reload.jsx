import React, {Fragment, useState} from 'react';
import styles from './Reload.module.css'
import TransactionList from "./TransactionList";

function Reload(props) {

    const [transactions, saveTransactions] = useState([]);

    const  getTransactions = async ()=>{
        console.log("calling Transactions API");
        const url = "http://localhost:8080/accounts/transactions"
        const response = await fetch(url);
        const transactions = await response.json();
        saveTransactions(transactions);

    };

    return (
        <Fragment>
        <div className={`${styles.reloader} row`}>
            <div className="col s12 m8 offset-m2">
                <form>
                    <h2 className={styles.heading}>Transactions</h2>
                    <div className="input-field col s12">
                        <input
                        type="button"
                        className={`${styles.btn_block} btn-large amber darken-2`}
                        value="Find transactions"
                        onClick={()=> getTransactions()}
                        />
                    </div>
                </form>
            </div>
        </div>
            <div className={`${styles.reloader} row`}>

            </div>
            <div className="col s12 m8 offset-m2">
            <TransactionList
                transactions={transactions}
            />
            </div>
        </Fragment>


);
}

export default Reload;
