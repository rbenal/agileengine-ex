import React, {Fragment} from 'react';
// import styles from './Transaction.module.css'

function Transaction({transaction}) {
    const {id, effectiveDate, amount, type} = transaction;

    return (
    <Fragment>
        {/*<div className="divider">*/}

        {/*</div>*/}
        {/*<div className={`section`}>*/}
            {/*<h5 className={`${type === "debit" ? styles.debit : styles.credit}`}> {type} ${amount}</h5>*/}
            {/*<div*/}
            {/*    className={`${styles.info_on}`}*/}
            {/*>*/}
            {/*<p>id: {id}</p>*/}
            {/*<p>date: {effectiveDate}</p>*/}
            {/*</div>*/}
            <div className="collapsible-header"><i className="material-icons">{type}</i>{amount}
            </div>
            <div className="collapsible-body">
                <span>{id}</span>
                <span>{effectiveDate}</span>
            </div>


        {/*</div>*/}
    </Fragment>
    );
}

export default Transaction;
