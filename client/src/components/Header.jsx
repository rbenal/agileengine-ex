import React from 'react';

function Header({title}) {
    return (
        <nav className="nav-wrapper light-blu darken-3">
            <a href="#!" className="brand-logo center">{title}</a>
        </nav>
    );
}

export default Header;
