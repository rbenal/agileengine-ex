import React, {useEffect, Fragment} from 'react';
import Transaction from "./Transaction";
import 'materialize-css/dist/css/materialize.min.css'
import 'materialize-css/dist/js/materialize.min'
import styles from './Transaction.module.css'



function TransactionList({transactions}) {
    useEffect(() => {
            console.log("addEventListener");
            var options = {};
            var elems = document.querySelectorAll('.collapsible');
            var instances = window.M.Collapsible.init(elems, options);

    }, []);


    return (
        <div className="row">
            <div className="col-12">
                <ul className="collapsible">
                        {
                            transactions.map(transaction => {
                                const {id, effectiveDate, amount, type} = transaction;
                                console.log(transaction);
                                return <li key={id}>
                                    <div className="collapsible-header">
                                        <i className="material-icons">attach_money</i>
                                        <span className={`${type === "debit" ? styles.debit : styles.credit}`}>{type}: ${amount}</span>

                                    </div>
                                    <div className="collapsible-body">
                                        <p>id: {id} </p>
                                        <p>date: {effectiveDate}</p>
                                    </div>
                                    </li>

                                // return <span key={tra}>1</span>
                                // return <Transaction
                                //     key={transaction.id}
                                //     transaction={transaction}
                                //
                                // />
                            })
                        }

                </ul>
            </div>
        </div>
    );
}

export default TransactionList;
