import React, {Fragment} from 'react';
import Header from "./components/Header";
import Reload from "./components/Reload";

function App() {



    return (
        <Fragment>
            <Header
            title="AgileEngine Ex"
            />

            <div className="container white">
              <Reload/>
            </div>


        </Fragment>

    );
}

export default App;
