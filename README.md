Total time 5 hours.

React APP is in folder: client
Binaries for react App: client/build
you have to install a server to running app from binaries

example: npm install -g serve

to run in development mode you have to install nodejs and npm and clone this repo, go to root folder and type:

npm install && npm run dev

please add some transactions before list it in the web page

Postman Collection file with examples is incluededin file: Postman.json

Important: this project does not implement a lock resource pattern, this project implements a queue to control concurrency, 
even read transactions are considered as a transactions to ensure complete all items in the exercise, this way i consider more efficient and
more readable code.




