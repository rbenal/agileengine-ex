class Transaction {

    id; //transaction id number
    effectiveDate; // transaction date
    amount; // transaction amount double
    type; // type of transaction [debit, credit]

    constructor(amount, type) {
        this.amount = amount;
        this.type = type;

    }

    //execute the transaction object
    exec() {

        //read operations are considering as a transaction
        if(this.type==='read')
            return _g_signedInUser;

        let factor = this.type === 'credit' ? 1 : -1;
        this.id = _g_transactioner._getNextTranId();
        _g_signedInUser.account.balance += (this.amount * factor);
        this.effectiveDate = new Date();

        //adding movement to accounts transactions array
        _g_signedInUser.account.transactions.push(this);
        return this;
    }
}
//class to execute transactions
class Transactioner {

    constructor() {
        console.log("new _g_transactioner instance");
        this._resourceLocked = false;
        this._transactionIdSequence = 1000
    };

    _transactionIdSequence;

    /**
     *  save a sequence id number for all transactions
     * @returns {*}
     * @private
     */
    _getNextTranId() {
        this._transactionIdSequence++;
        return this._transactionIdSequence;
    }

    //read operation as transactions
    async _read(amount) {
        const action = 'read';
        let readTransaction;
        console.log("adding read transaction to queue");
        await new Promise((resolve, reject) => {
            _g_queue.push(function (cb) {
                setTimeout(() => {
                    console.log('new read transaction executed');
                    readTransaction = new Transaction(amount, action).exec();
                    resolve();
                }, 0);
            });
        });
        return readTransaction;
    }

    //credit operation as transactions
    async _credit(amount) {
        const action = 'credit';
        console.log("adding credit transaction to queue");
        let creditTransaction;
        await new Promise((resolve, reject) => {
            _g_queue.push(function (cb) {
                setTimeout(() => {
                    console.log('new credit transaction executed');
                    creditTransaction = new Transaction(amount, action).exec();
                    resolve();
                }, 0);
            });
        });
        return creditTransaction;
    }
    //debit operation as transactions
    async _debit(amount) {
        const action = 'debit';
        /**
         * Simulate debit transactions before execute
         * @param amount
         * @returns {boolean}
         */
        let simulateDebit = (amount) => {
            console.log('simulating transaction');
            amount = Number.parseFloat(amount);
            let balance = _g_signedInUser.account.balance;
            balance -= amount;
            return balance >= 0;
        };
        if (simulateDebit(amount)) {
            // _g_signedInUser.account.balance -= amount;
            console.log("adding debit transaction to queue");
            let debitTransaction;
            await new Promise((resolve, reject) => {
                _g_queue.push(function (cb) {
                    setTimeout(() => {
                        debitTransaction = new Transaction(amount, action).exec();
                        console.log('new debit transaction executed');
                        resolve();
                    }, 0);
                });
            });
            return debitTransaction;
        } else {
            return false;
        }

    }

    run(t) {
        if (t.type === 'credit')
            return this._credit(t.amount);
        else if (t.type === 'debit') {
            return this._debit(t.amount);
        } else if (t.type === 'read') {
            return this._read();
        }

    }

}

//singleton class to ensure there will be a unique transactioner instance
class Singleton {
    static getNewTransactionerInstance() {

        return new Transactioner();
    }

    constructor() {
        console.log('_g_transactioner constructor');
        if (!this.instance) {
            this.instance = Singleton.getNewTransactionerInstance();
            console.log('_g_transactioner constructor inside');
        }
    }
}

module.exports = new Singleton().instance;
