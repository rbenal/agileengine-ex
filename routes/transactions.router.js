let express = require('express');
let router = express.Router();



/* GET transactions listing. */
router.get('/', function (req, res, next) {
    res.send(_g_signedInUser.account.transactions);
});

/* GET transaction by ID. */
router.get('/:id', function (req, res, next) {
    console.log(req.params);
    res.send(_g_signedInUser.account.transactions.find(trans => {
            return trans.id === Number.parseInt(req.params.id);
        }
    ));
});

/* POST a new transaction. */
router.post('/', function (req, res, next) {
    let t = _g_transactioner.run(req.body);
    t.then((result) => {
        if (!result) {
            res.statusCode = 422;
            res.send({error: 'insufficient balance'});
        } else {
            res.send(result);
        }
    });

});

module.exports = router;
