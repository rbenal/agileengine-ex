//import queue library
let queue = require('queue');

//creating queue if this does not exist
if (!global._g_queue) {
    global._g_queue = queue();
    _g_queue.autostart = true;
    console.log("queue created");
}
//adding transactioner to globals, there must be oa unique transactioner, singleton pattern implemented
global._g_transactioner = require('./classes/transactioner');
console.log("transactioner created");

//initial Account Configuration
global._g_signedInUser = {
    userId: 123456,
    account: {
        accountNumber: 123456789,
        balance: 1000,
        transactions: []

    }
};
console.log('Account data loaded.');


